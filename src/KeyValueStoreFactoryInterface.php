<?php

namespace Drupal\keyvalue_store;

/**
 * Interface KeyValueStoreStorageInterface
 *
 * @package Drupal\keyvalue_store
 */
interface KeyValueStoreFactoryInterface {

  /**
   * @return string
   */
  public function getServiceClass();

}
