<?php

namespace Drupal\keyvalue_store;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;

/**
 * Class KeyValueRedisFactory
 *
 * @package Drupal\keyvalue_redis
 */
abstract class KeyValueFactoryAbstract implements KeyValueFactoryInterface,KeyValueStoreFactoryInterface {

  /**
   * The client factory.
   *
   * @var mixed
   */
  protected $clientFactory;

  /**
   * The serialization class to use.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $serializer;

  /**
   * List of collections.
   *
   * @var array
   */
  protected $collections = [];

  /**
   * Creates a Generic KeyValueFactory.
   *
   * @param mixed $client_factory
   *   The redis client factory.
   * @param \Drupal\Component\Serialization\SerializationInterface $serializer
   *   The serialization class to use.
   */
  public function __construct($client_factory, SerializationInterface $serializer) {
    $this->clientFactory = $client_factory;
    $this->serializer = $serializer;
  }

  /**
   * {@inheritdoc}
   */
  public function get($collection) {
    if (!isset($this->collections[$collection])) {
      $class_name = $this->getServiceClass();
      $this->collections[$collection] = new $class_name($collection, $this->serializer, $this->clientFactory);
    }
    return $this->collections[$collection];
  }

}
