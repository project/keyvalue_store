<?php

namespace Drupal\keyvalue_store;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\KeyValueStore\DatabaseStorage;
use Drupal\Core\KeyValueStore\StorageBase;
use Drupal\Core\Site\Settings;

/**
 * Class KeyValueBaseAbstract
 *
 * @package Drupal\keyvalue_store
 */
abstract class KeyValueBaseAbstract extends StorageBase implements KeyValueStoreStorageInterface{

  /**
   * @var \Drupal\Core\KeyValueStore\DatabaseStorage
   */
  protected $keyValueDatabase;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * @var mixed
   */
  protected $clientFactory;

  /**
   * @var $mixed
   */
  protected $client;

  /**
   * The serialization class to use.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $serializer;

  /**
   * Flag to determining if DB is used as backup for store key/value.
   *
   * @var bool
   */
  protected $backup;

  /**
   * All data from the database collection.
   *
   * @var []
   */
  protected $data;

  /**
   * All keys from the database collection.
   *
   * @var []
   */
  protected $keys;

  /**
   * Overrides Drupal\Core\KeyValueStore\StorageBase::__construct().
   *
   * @param string $collection
   *   The name of the collection holding key and value pairs.
   * @param \Drupal\Component\Serialization\SerializationInterface $serializer
   *   The serialization class to use.
   * @param $client
   */
  public function __construct($collection, SerializationInterface $serializer, $client_factory) {
    parent::__construct($collection);
    $this->serializer = $serializer;
    $this->clientFactory = $client_factory;
    $this->client = $this->getServiceClient();
    $this->backup = $this->getServiceType() === self::KEY_VALUE_STORE && !Settings::get('keyvalue_store_nodb');
  }

  /**
   * {@inheritdoc}
   */
  public function has($key) {
    return !empty($this->get($key));
  }

  /**
   * {@inheritdoc}
   */
  public function get($key, $default = NULL) {
    $values = $this->getMultiple([$key]);
    return $values[$key] ?? $default;
  }

  /**
   * {@inheritdoc}
   */
  public function set($key, $value) {
    if ($this->backup) {
      $this->keyValueDatabase()->set($key, $value);
    }
    $this->setExternal($key, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function setIfNotExists($key, $value) {
    if ($this->backup) {
      $this->keyValueDatabase()->setIfNotExists($key, $value);
    }
    $this->setIfNotExistsExternal($key, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function setMultiple(array $data) {
    if ($this->backup) {
      $this->keyValueDatabase()->setMultiple($data);
    }
    $this->setMultipleExternal($data);
  }

  /**
   * {@inheritdoc}
   */
  public function rename($key, $new_key) {
    if ($this->backup) {
      $this->keyValueDatabase()->rename($key, $new_key);
    }
    $this->renameExternal($key, $new_key);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultiple(array $keys) {
    if ($this->backup) {
      $this->keyValueDatabase()->deleteMultiple($keys);
    }
    $this->deleteMultipleExternal($keys);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll() {
    if ($this->backup) {
      $this->keyValueDatabase()->deleteAll();
    }
    $this->deleteAllExternal();
  }

  /**
   * @return array
   */
  protected function getAllDatabaseKeys() {
    if (!isset($this->keys[$this->collection])) {
      $this->keys[$this->collection] = $this->database()->query('SELECT name, name FROM {key_value} WHERE collection = :collection', [':collection' => $this->collection])->fetchAllKeyed();
    }

    return $this->keys[$this->collection];
  }

  /**
   * @return array
   */
  public function getAllDatabaseStorage() {
    if (!isset($this->data[$this->collection])) {
      $this->data[$this->collection] = $this->keyValueDatabase()->getAll();
    }

    return $this->data[$this->collection];
  }

  /**
   * {@inheritdoc}
   */
  public function matchDatabaseStorage(array $keys) {
    // Fetch keys to be much faster. In most cases
    // we don't need entire data, just keys to verify if
    // we have specific entry in DB but not in maybe volatile storages
    // which are not so persistent (memcache).
    $db_keys = $this->getAllDatabaseKeys();

    if (!empty(array_intersect($keys, array_keys($db_keys)))) {
      $data = $this->getAllDatabaseStorage();
      // Set only external storage. Avoid double writing on
      // core database.
      $this->setMultipleExternal($data);
      return TRUE;
    }

    return FALSE;
  }

  /**
   * @param $key
   * @param $value
   *
   * @return array
   */
  protected function createEntryHash($key, $value) {
    return [
      'key' => $key,
      'data' => $this->serializer->encode($value),
    ];
  }

  protected function getServiceClient() {
    return NULL;
  }

  /**
   * @return \Drupal\Core\KeyValueStore\DatabaseStorage
   */
  protected function keyValueDatabase() {
    if (!isset($this->keyValueDatabase)) {
      $this->keyValueDatabase = new DatabaseStorage($this->collection, $this->serializer, $this->database());
    }

    return $this->keyValueDatabase;
  }

  /**
   * @return \Drupal\Core\Database\Connection
   */
  protected function database() {
    if (!isset($this->database)) {
      $this->database = \Drupal::database();
    }

    return $this->database;
  }

}
