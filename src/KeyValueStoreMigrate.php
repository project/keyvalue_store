<?php

namespace Drupal\keyvalue_store;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\keyvalue_redis\KeyValueStore\RedisStorage;
use Drupal\keyvalue_redis\KeyValueStore\RedisStorageExpirable;
use Drupal\keyvalue_memcache\KeyValueStore\MemcacheStorage;
use Drupal\keyvalue_memcache\KeyValueStore\MemcacheStorageExpirable;

/**
 * Migrates entries from DB to external provider.
 *
 * @package Drupal\keyvalue_store
 */
class KeyValueStoreMigrate {

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The serialization class to use.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $serializer;

  /**
   * KeyValueStoreMigrate constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\Component\Serialization\SerializationInterface $serializer
   */
  public function __construct(Connection $database, ModuleHandlerInterface $module_handler, SerializationInterface $serializer) {
    $this->database = $database;
    $this->moduleHandler = $module_handler;
    $this->serializer = $serializer;
  }

  /**
   * Migrate data from database to external destination.
   *
   * @param string $destination
   *   Type of service where we want to migrate data.
   * @param $type
   *   Type of key value storage, regular or expire.
   */
  public function run($service, $type) {

    $clients = $this->mapClients();

    // Basic sanity checks.
    if (!isset($clients[$service]) || !in_array($type, ['key_value', 'key_value_expire'])) {
      return;
    }

    $destination = $clients[$service];

    $key_values = $this->database->select($type, 'k')
        ->fields('k', [])
        ->execute()->fetchAll();

    // Keep per collection in array to reduce initialization multiple
    // time for same collection.
    $connectors = [];

    foreach ($key_values as $id => $key_value) {

      $collection = $key_value->collection;
      $name = $key_value->name;
      $value = $this->serializer->decode($key_value->value);
      $expire = $key_value->expire ?? 0;

      if (!isset($connectors[$collection])) {
        $class_name = $destination[$type];
        $connectors[$collection] = new $class_name($collection, $this->serializer, $destination['client']);
      }

      $connectors[$collection]->skip_backup = TRUE;

      if ($type === 'key_value') {
        // Use external, so that we don't write again in DB.
        $connectors[$collection]->setExternal($name, $value);
      } else {
        $connectors[$collection]->setWithExpire($name, $value, $expire);
      }
    }
  }

  /**
   * @return array[]
   */
  protected function mapClients() {
    $clients = [];

    if ($this->moduleHandler->moduleExists('redis') && $this->moduleHandler->moduleExists('keyvalue_redis')) {
      $clients['redis'] = [
        'client' => \Drupal::service('redis.factory')->getClient(),
        'key_value' => RedisStorage::class,
        'key_value_expire' => RedisStorageExpirable::class
      ];
    }

    if ($this->moduleHandler->moduleExists('memcache') && $this->moduleHandler->moduleExists('keyvalue_memcache')) {
      $clients['memcache'] = [
        'client' => \Drupal::service('memcache.factory'),
        'key_value' => MemcacheStorage::class,
        'key_value_expire' => MemcacheStorageExpirable::class
      ];
    }

    return $clients;
  }

}
