<?php

namespace Drupal\keyvalue_store;

/**
 * Interface KeyValueStoreStorageInterface
 *
 * @package Drupal\keyvalue_store
 */
interface KeyValueStoreStorageInterface {

  const KEY_VALUE_STORE = 'key_value';
  const KEY_VALUE_EXPIRE_STORE = 'key_value_expire';

  /**
   * Return type of the service.
   *
   * @return string
   */
  public function getServiceType();

  /**
   * Fetches all keys from database.
   *
   * @return array
   */
  public function getAllKeys();

  /**
   * @return mixed
   */
  public function matchDatabaseStorage(array $keys);

  /**
   * Saves a value for a given key.
   *
   * @param string $key
   *   The key of the data to store.
   * @param mixed $value
   *   The data to store.
   */
  public function setExternal($key, $value);

  /**
   * Saves a value for a given key if it does not exist yet.
   *
   * @param string $key
   *   The key of the data to store.
   * @param mixed $value
   *   The data to store.
   *
   * @return bool
   *   TRUE if the data was set, FALSE if it already existed.
   */
  public function setIfNotExistsExternal($key, $value);

  /**
   * Saves key/value pairs.
   *
   * @param array $data
   *   An associative array of key/value pairs.
   */
  public function setMultipleExternal(array $data);

  /**
   * Renames a key.
   *
   * @param string $key
   *   The key to rename.
   * @param string $new_key
   *   The new key name.
   */
  public function renameExternal($key, $new_key);

  /**
   * Deletes an item from the key/value store.
   *
   * @param string $key
   *   The item name to delete.
   */
  public function deleteExternal($key);

  /**
   * Deletes multiple items from the key/value store.
   *
   * @param array $keys
   *   A list of item names to delete.
   */
  public function deleteMultipleExternal(array $keys);

  /**
   * Deletes all items from the key/value store.
   */
  public function deleteAllExternal();

}
