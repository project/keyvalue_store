<?php

namespace Drupal\keyvalue_redis\KeyValueStore;

use Drupal\keyvalue_store\KeyValueFactoryAbstract;

/**
 * A key value factory implementation for redis.
 *
 * @package Drupal\keyvalue_redis
 */
class KeyValueRedisFactory extends KeyValueFactoryAbstract {

  /**
   * @return string
   */
  public function getServiceClass() {
    return RedisStorage::class;
  }

}
