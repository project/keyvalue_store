<?php

namespace Drupal\keyvalue_redis\KeyValueStore;

/**
 * A key value expirable factory implementation for redis.
 */
class KeyValueExpirableRedisFactory extends KeyValueRedisFactory {

  /**
   * @return string
   */
  public function getServiceClass() {
    return RedisStorageExpirable::class;
  }

}
