<?php

namespace Drupal\keyvalue_redis\KeyValueStore;

use Drupal\keyvalue_store\KeyValueBaseAbstract;
use Drupal\keyvalue_store\KeyValueStoreStorageInterface;
use Drupal\redis\RedisPrefixTrait;

/**
 * Class RedisStorage
 *
 * @package Drupal\keyvalue_redis
 */
class RedisStorage extends KeyValueBaseAbstract {

  use RedisPrefixTrait;

  /**
   * {@inheritdoc}
   */
  public function getServiceType() {
    return KeyValueStoreStorageInterface::KEY_VALUE_STORE;
  }

  /**
   * {@inheritdoc}
   */
  public function getMultiple(array $keys) {
    if (empty($keys)) {
      return [];
    }

    $return = [];

    // Build the list of keys to fetch.
    $redis_keys = array_map([$this, 'getKey'], $keys);

    // Optimize for the common case when only a single cache entry needs to
    // be fetched, no pipeline is needed then.
    if (count($redis_keys) > 1) {
      $pipe = $this->client->multi(\Redis::PIPELINE);
      foreach ($redis_keys as $key) {
        $pipe->hgetall($key);
      }
      $result = $pipe->exec();
    }
    else {
      $result = [$this->client->hGetAll(reset($redis_keys))];
    }

    foreach ($result as $item) {
      if (!empty($item)) {
        $return[$item['key']] = $this->serializer->decode($item['data']);
      }
    }

    // For people who maybe decide to run this in not persistent Redis
    // setup.
    // If there is missing entry for key_value only, check in DB.
    if ($this->backup && empty($return) && $this->matchDatabaseStorage($keys)) {
      $this->getMultiple($keys);
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function getAll() {
    return $this->getMultiple($this->getAllKeys());
  }

  /**
   * {@inheritdoc}
   */
  public function setExternal($key, $value) {
    return $this->setMultipleExternal([$key => $value]);
  }


  /**
   * {@inheritdoc}
   */
  public function setMultipleExternal(array $data) {
    $pipe = $this->client->multi(\Redis::PIPELINE);
    foreach ($data as $key => $value) {
      $set_key = $this->getKey($key);
      $hash = $this->createEntryHash($key, $value);
      $pipe->hMset($set_key, $hash);
    }
    return (bool) $pipe->exec();
  }

  /**
   * {@inheritdoc}
   */
  public function setIfNotExistsExternal($key, $value) {
    if (!$this->has($key)) {
      return $this->setExternal($key, $value);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function renameExternal($key, $new_key) {
    // We cannot use \Redis::rename() as it does not support renaming hashes.
    // @todo Why doesn't the base class do this for us?
    if ($value = $this->get($key)) {
      $value[$key] = $new_key;
      $this->set($this->getKey($new_key), $value);
      $this->delete($key);
    }
  }

  public function deleteExternal($key) {
    $this->deleteMultipleExternal([$key]);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultipleExternal(array $keys) {
    // Build the list of keys to fetch.
    $redis_keys = array_map([$this, 'getKey'], $keys);

    $pipe = $this->client->multi(\Redis::PIPELINE);
    foreach ($redis_keys as $key) {
      $pipe->del($key);
    }
    $pipe->exec();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAllExternal() {
    $this->deleteMultipleExternal($this->getAllKeys());
  }

  /**
   * @return mixed
   */
  public function getAllKeys() {
    if ($this->backup && $this->getServiceType() === self::KEY_VALUE_STORE) {
      return $this->getAllDatabaseKeys();
    }

    $key_prefix = $this->getKey();
    $keys = [];
    do {
      if ($results = $this->client->scan($iterator, $key_prefix . '*', 1000)) {
        $keys = array_merge($keys, $results);
      }
    } while ($iterator);

    return array_map([$this, 'unPrefixKey'], $keys);
  }

  /**
   * Reverse the affects of ::getKey() by un-prefixing a key.
   *
   * @param string $key
   *   The prefixed key.
   *
   * @return string
   *   The original un-prefixed key.
   */
  public function unPrefixKey($key) {
      return str_replace($this->getKey(), '', $key);
  }

  /**
   * Return the redis key for the given key.
   *
   * @param string $key
   *   The key to prefix.
   *
   * @return string
   *   The full Redis key including the prefix.
   */
  protected function getKey($name = NULL) {
    if ($name) {
      return $this->getPrefix() . ':' . $this->getServiceType() . ':' . $this->collection . ':' . $name;
    }

    return $this->getPrefix() . ':' . $this->getServiceType() . ':' . $this->collection . ':';
  }

  /**
   * @return mixed
   */
  public function getServiceClient() {
    return $this->clientFactory->getClient();
  }

}
