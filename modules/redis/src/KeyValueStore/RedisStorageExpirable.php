<?php

namespace Drupal\keyvalue_redis\KeyValueStore;

use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\keyvalue_store\KeyValueStoreStorageInterface;
use Drupal\redis\RedisPrefixTrait;

/**
 * Class RedisStorageExpirable
 *
 * @package Drupal\keyvalue_redis
 */
class RedisStorageExpirable extends RedisStorage implements KeyValueStoreExpirableInterface {

  use RedisPrefixTrait;

  /**
   * {@inheritdoc}
   */
  public function getServiceType() {
    return KeyValueStoreStorageInterface::KEY_VALUE_EXPIRE_STORE;
  }

  /**
   * {@inheritdoc}
   */
  public function setWithExpire($key, $value, $expire) {
    return $this->setMultipleWithExpire([$key => $value], $expire);
  }

  /**
   * {@inheritdoc}
   */
  public function setWithExpireIfNotExists($key, $value, $expire) {
    if (!$this->has($key)) {
      return $this->setMultipleWithExpire([$key => $value], $expire);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setMultipleWithExpire(array $data, $expire) {
    $pipe = $this->client->multi(\Redis::PIPELINE);
    foreach ($data as $key => $value) {
      $set_key = $this->getKey($key);
      $hash = $this->createEntryHash($key, $value);
      $pipe->hMset($set_key, $hash);
      $pipe->pExpire($set_key, $expire * 1000);
    }
    return (bool) $pipe->exec();
  }

}
