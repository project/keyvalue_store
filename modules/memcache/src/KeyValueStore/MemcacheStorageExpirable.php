<?php

namespace Drupal\keyvalue_memcache\KeyValueStore;

use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\keyvalue_store\KeyValueStoreStorageInterface;

/**
 * Class MemcacheStorageExpirable
 *
 * @package Drupal\keyvalue_memcache
 */
class MemcacheStorageExpirable extends MemcacheStorage implements KeyValueStoreExpirableInterface {

  /**
   * {@inheritdoc}
   */
  public function getServiceType() {
    return KeyValueStoreStorageInterface::KEY_VALUE_EXPIRE_STORE;
  }

  /**
   * {@inheritdoc}
   */
  public function setWithExpire($key, $value, $expire) {
    $this->client->set($this->getKey($key), $this->createEntryHash($key, $value), $expire);
    $this->trackCollectionKeys($key);
  }

  /**
   * {@inheritdoc}
   */
  public function setWithExpireIfNotExists($key, $value, $expire) {
    $this->trackCollectionKeys($key);
    return $this->client->add($this->getKey($key), $this->createEntryHash($key, $value), $expire);
  }

  /**
   * {@inheritdoc}
   */
  public function setMultipleWithExpire(array $data, $expire) {
    foreach ($data as $key => $value) {
     $this->setWithExpire($key, $value, $expire);
    }
  }

}
