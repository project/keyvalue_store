<?php

namespace Drupal\keyvalue_memcache\KeyValueStore;

/**
 * A key value expirable factory implementation for memcache.
 */
class KeyValueExpirableMemcacheFactory extends KeyValueMemcacheFactory {

  /**
   * @return string
   */
  public function getServiceClass() {
    return MemcacheStorageExpirable::class;
  }

}
