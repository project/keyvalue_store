<?php

namespace Drupal\keyvalue_memcache\KeyValueStore;

use Drupal\keyvalue_store\KeyValueBaseAbstract;
use Drupal\keyvalue_store\KeyValueStoreStorageInterface;

/**
 * Class MemcacheStorage
 *
 * @package Drupal\keyvalue_memcache
 */
class MemcacheStorage extends KeyValueBaseAbstract {

  /**
   * @var []
   */
  protected $collectionMapping;

  /**
   * {@inheritdoc}
   */
  public function getServiceType() {
    return KeyValueStoreStorageInterface::KEY_VALUE_STORE;
  }

  /**
   * {@inheritdoc}
   */
  public function getMultiple(array $keys) {
    if (empty($keys)) {
      return [];
    }

    $return = [];

    // Build the list of keys to fetch.
    $memcache_keys = array_map([$this, 'getKey'], $keys);

    // Optimize for the common case when only a single cache entry needs to
    // be fetched, no pipeline is needed then.
    if (count($memcache_keys) > 1) {
      $result = $this->client->getMulti($memcache_keys);
    }
    else {
      $result = [$this->client->get(reset($memcache_keys))];
    }

    foreach ($result as $item) {
      if (isset($item['key'])) {
        $return[$item['key']] = $this->serializer->decode($item['data']);
      }
    }

    // Memcache is not known for persistence as Redis.
    // If there is missing entry for key_value only, check in DB.
    if ($this->backup && empty($return) && $this->matchDatabaseStorage($keys)) {
      $this->getMultiple($keys);
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function getAll() {
    $keys = $this->getAllKeys();
    if (empty($keys)) {
      return [];
    }
    return $this->getMultiple($keys);
  }

  /**
   * {@inheritdoc}
   */
  public function setExternal($key, $value) {
    $this->trackCollectionKeys($key);
    return $this->client->set($this->getKey($key), $this->createEntryHash($key, $value));
  }


  /**
   * {@inheritdoc}
   */
  public function setMultipleExternal(array $data) {
    foreach ($data as $key => $value) {
     $this->setExternal($key, $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setIfNotExistsExternal($key, $value) {
    $this->trackCollectionKeys($key);
    return $this->client->add($this->getKey($key), $this->createEntryHash($key, $value));
  }

  /**
   * {@inheritdoc}
   */
  public function renameExternal($key, $new_key) {
    if ($value = $this->client->get($this->getKey($key))) {
      $value[$key] = $new_key;
      $this->client->set($this->getKey($new_key), $value);
      $this->client->delete($this->getKey($key));
      $this->trackCollectionKeys($new_key);
      $this->trackCollectionKeys($key, TRUE);
    }
  }

  public function deleteExternal($key) {
    $this->client->delete($this->getKey($key));
    $this->trackCollectionKeys($key, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultipleExternal(array $keys) {
    // Build the list of keys to fetch.
    $memcache_keys = array_map([$this, 'getKey'], $keys);

    foreach ($memcache_keys as $key) {
      $this->deleteExternal($key);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAllExternal() {
    $keys = $this->getAllKeys();
    // Build the list of keys to fetch.
    $memcache_keys = array_map([$this, 'getKey'], $keys);

    foreach ($memcache_keys as $key) {
      $this->delete($key);
    }
  }

  /**
   * @return mixed
   */
  public function getAllKeys() {
    if ($this->backup && $this->getServiceType() === self::KEY_VALUE_STORE) {
      return $this->getAllDatabaseKeys();
    }

    // We can't preform any kind of scan in memcache.
    // Tracking keys mapped in array is not end well with
    // expire storage and form / form state values. It's gonna break
    // eventually - skipping form / form_state collection tracking.
    // TODO: either find efective way, or remove Expire implementation.
   return $this->getCollectionKeys();
  }

  /**
   * Return the memcache key for the given key.
   *
   * @param string $key
   *   The key to prefix.
   *
   * @return string
   *   The memcache key without the prefix.
   */
  protected function getKey($key) {
    return $this->getServiceType() . ':' . $this->collection . ':' . $key;
  }

  /**
   * {@inheritdoc}
   */
  protected function getServiceClient() {
    return $this->clientFactory->get($this->getServiceType());
  }

  /**
   * @return mixed
   */
  protected function getCollectionKeys() {
    if (!isset($this->collectionMapping[$this->collection])) {
      $this->collectionMapping[$this->collection] = $this->client->get($this->collectionKey());
    }

    return $this->collectionMapping[$this->collection] ?? [];
  }

  /**
   * @param $cid
   *
   * @return bool
   */
  protected function trackCollectionKeys($cid, $unset = FALSE) {
    if ($this->collection === 'form_state' || $this->collection === 'form') {
      return FALSE;
    }
    $data = $this->getCollectionKeys();

    if (!$unset) {
      $data[$cid] = $cid;
    } else {
      unset($data[$cid]);
    }

    return $this->client->set($this->collectionKey(), $data);
  }

  /**
   * @return string
   */
  protected function collectionKey() {
    return $this->collection . ':mapping:' . $this->getServiceType();
  }

}
