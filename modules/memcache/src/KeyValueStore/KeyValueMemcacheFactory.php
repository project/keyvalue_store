<?php

namespace Drupal\keyvalue_memcache\KeyValueStore;

use Drupal\keyvalue_store\KeyValueFactoryAbstract;

/**
 * A key value factory implementation for memcache.
 *
 * @package Drupal\keyvalue_memcache
 */
class KeyValueMemcacheFactory extends KeyValueFactoryAbstract {

  /**
   * @return string
   */
  public function getServiceClass() {
    return MemcacheStorage::class;
  }

}
